.. _reStructuredText_commands:

#########################################
COMANDI DEL LINGUAGGIO `reStructuredText`
#########################################
In questa sezione si forniranno  indicazioni di carattere generale per la formattazione richiesta dal linguaggio *reStructuredText* ed in particolare dal programma **SPHINX** per ottenere la formattazione del testo desiderata.

.. NOTE::

   |notice| Il codice per ottenere quanto descritto viene inserito all'interno degli appositi **box**.

Visitare <https://docutils.sourceforge.io/rst.html> per ulteriori informazioni.

******************************
MESSAGGI DI AVVISO E SICUREZZA
******************************
Si possono inserire messaggi di avviso e sicurezza all'interno di appositi **box**. Per la spiegazione del significato da attribuire vedere la :numref:`avviso_sicurezza`.

Le tipologie possibili sono:

* Nota;
* Avvertimento;
* Attenzione;
* Pericolo.

.. code-block:: rst

   .. NOTE::

      |notice| Viene utilizzato per affrontare le pratiche non legate a lesioni fisiche.

   .. WARNING::

      |warning| Indica una situazione di rischio potenziale che, se non prevista, potrebbe causare danni di minore o modesta entità.

   .. CAUTION::

      |caution| Indica una situazione di rischio potenziale che, se non evitata, può causare morte o danno grave.

   .. DANGER::

      |danger| Indica una situazione di rischio imminente che, se non evitata, causa morte o danno grave.


***********************
Struttura delle sezioni
***********************
In base alla tipologia i titoli di sezione sono solo sottolineati oppure sono anche sopralineati.

.. code-block:: rst

   #####
   Parte
   #####

   *******
   Capitolo
   *******

   Sezione
   =======

   Sottosezione
   ------------

   Sotto sottosezione
   ^^^^^^^^^^^^^^^^^^

   Paragrafo
   """""""""

I simboli utilizzati devono coprire almeno la lunghezza del testo del titolo.

*******************
Enfasi delle parole
*******************
Si possono ottenere parole in:

* *corsivo*;
* **grassetto**;
* `testo interpretato`;
* ``testo letterale``;
* hyperlink singolo es: `reStructuredText <https://docutils.sourceforge.io/rst.html>`_.

.. code-block:: rst

   *corsivo*;
   **grassetto**;
   `testo interpretato`;
   ``testo letterale``;
   hyperlink singolo es: `reStructuredText <https://docutils.sourceforge.io/rst.html>`_.

* nota a piè di pagina [#name]_;

.. code-block:: rst

   nota a piè di pagina [#name]_;
   .
   .
   .

   .. rubric:: Footnotes

   .. [#name] Testo di prova della nota a piè di pagina.

* citazione [CIT001]_

.. code-block:: rst

   .
   .
   .
   .. [CIT001] Prova di una citazione.

.. _elenchi:

*******
Elenchi
*******

.. _elenco_puntato:

Elenco puntato
==============
Esempio di elenco puntato:

* punto A
* punto B
* punto C

   * punto rientrato

      * punto rientrato

.. code-block:: rst

   * punto A
   * punto B
   * punto C

      * punto rientrato

         * punto rientrato

Per ottenere una indentazione è necessario lasciare una riga vuota sia prima che dopo la lista e inserire 3 caratteri di spazio.

.. _elenco_numerato:

Elenco numerato
===============
Esempio di elenco numerato:

#. Step 1

   a. Step a
   #. Step b

#. Step 2
#. Step 3

Liste indentate all'interno di liste numerate sono rese con elenchi puntati o, se si utilizzano elenchi numerati, con numerazione letterale.

Per la numerazione automatica utilizzare ``#.``. Si può iniziare una lista anche con una lettera ``a.`` e poi utilizzare la numerazione automatica.

.. code-block:: rst

   #. Step 1

      a. Step a
      #. Step b

   #. Step 2
   #. Step 3

*******
Tabelle
*******
Tabella semplice
================
Tabelle semplici possono essere inserite con:

.. code-block:: rst

   +-------+-------+-------+
   |   1   |   2   |   3   |
   +-------+-------+-------+

+-------+-------+-------+
|   1   |   2   |   3   |
+-------+-------+-------+

Tabella complessa
=================
Metodo a sintassi diretta
-------------------------

.. code-block:: rst

   +----------------+------------------+------------------+
   | Intestazione 1 | Intestazione 2   | Intestazione 3   |
   +================+==================+==================+
   | riga 1         | colonna 2        | colonna 3        |
   +----------------+------------------+------------------+
   | riga 2         |          riga multi colonna         |
   +----------------+------------------+------------------+
   | riga 3         | Cella            | * Cells          |
   +----------------+ multi            | * contain        |
   | riga 4         | riga             | * blocks.        |
   +----------------+------------------+------------------+


+----------------+------------------+------------------+
| Intestazione 1 | Intestazione 2   | Intestazione 3   |
+================+==================+==================+
| riga 1         | colonna 2        | colonna 3        |
+----------------+------------------+------------------+
| riga 2         |          riga multi colonna         |
+----------------+------------------+------------------+
| riga 3         | Cella            | * Cells          |
+----------------+ multi            | * contain        |
| riga 4         | riga             | * blocks.        |
+----------------+------------------+------------------+

Metodo con utilizzo di *direttiva*
----------------------------------
Si può utilizzare il comando ``.. tabularcolumns::`` per definire la larghezza delle colonne e l'allineamento del contenuto cella in modo semplice:

.. code-block:: rst

   .. tabularcolumns:: |l|c|p{5cm}|

   +-----------------+-------+-------+
   |   simple text   |   2   |   3   |
   +-----------------+-------+-------+

.. tabularcolumns:: |l|c|p{5cm}|

+-----------------+-------+-------+
|   simple text   |   2   |   3   |
+-----------------+-------+-------+

Metodo con tabella *csv*
------------------------
Si può utilizzare una sintassi simile alle tabelle **.csv**.

.. code-block:: rst

   .. csv-table:: Titolo tabella
      :header: "cognome", "nome", "età"
      :widths: 20, 20, 10

      "Smith", "John", 40
      "Smith", "John, Junior", 20

.. csv-table:: Titolo tabella
   :header: "cognome", "nome", "età"
   :widths: 20, 20, 10

   "Smith", "John", 40
   "Smith", "John, Junior", 20

********
Immagini
********
Per inserire una immagine utilizzare:

.. code-block:: rst

   .. _nome_riferimento_interno:
   .. figure:: _static/logo_sisma.png
      :width: 6 cm
      :align: center

   Logo SISMA

.. _nome_riferimento_interno:
.. figure:: _static/logo_sisma.png
   :width: 6 cm
   :align: center

   Logo SISMA

.. |logo_sisma| image:: _static/logo_sisma.png
   :height: 1 cm
   :align: middle

Si può anche inserire una immagine in linea con il testo.

Per fare questo si deve definire una etichetta dell'immagine e poi richiamarla nel testo.

.. code-block:: rst

   .. |logo_sisma| image:: _static/logo_sisma.png
      :height: 1 cm
      :align: middle
 
   Il logo |logo_sisma| è il marchio dell'azienda.

Il logo |logo_sisma| è il marchio dell'azienda.

.. _riferimenti_incrociati:

**********************
Riferimenti incrociati
**********************
Utilizzando il nome riferimento è possibile creare riferimenti incrociati utilizzando la didascalia oppure il numero incrementale della figura.

.. code-block:: rst

   Il :ref:`nome_riferimento_interno` è la :numref:`nome_riferimento_interno`.

Il :ref:`nome_riferimento_interno` è la :numref:`nome_riferimento_interno`.

Identici comandi per i riferimenti alle sezioni.

.. code-block:: rst

   .. _riferimenti_incrociati:

   **********************
   Riferimenti incrociati
   **********************

   La sezione :ref:`riferimenti_incrociati` è la :numref:`riferimenti_incrociati`.

La sezione :ref:`riferimenti_incrociati` è la :numref:`riferimenti_incrociati`.

.. _riferimenti_incrociati_etichetta:

Si può mettere un nome riferimento a qualunque oggetto. Se non è una figura oppure una parte della struttura bisogna però riferirsi a quell'oggetto fornendo una etichetta.

.. code-block:: rst

   .. _riferimenti_incrociati_etichetta:

   Nel punto del documento :ref:`Codice per riferimenti incrociati con etichetta <riferimenti_incrociati_etichetta>` si spiegano i dettagli su come realizzare riferimenti incrociati a parti generiche del documento.

Nel punto del documento :ref:`Codice per riferimenti incrociati con etichetta <riferimenti_incrociati_etichetta>` si spiegano i dettagli su come realizzare riferimenti incrociati a parti generiche del documento.

.. _equazioni_matematiche:

*********************
Equazioni Matematiche
*********************
Si possono inserire anche equazioni e simboli matematici utilizzando il linguaggio *latex*. Si può inserire in linea con il testo:

.. code-block:: rst

   Se :math:`\alpha = 10` e :math:`\beta = 1` allora :math:`\alpha > \beta`.

Se :math:`\alpha = 10` e :math:`\beta = 1` allora :math:`\alpha > \beta`.

Oppure si posso inserire equazioni più complesse:
 
.. code-block:: rst

   .. math::

      n_{\mathrm{offset}} = \sum_{k=0}^{N-1} s_k n_k

.. math::

   n_{\mathrm{offset}} = \sum_{k=0}^{N-1} s_k n_k


.. [CIT001] Prova di una citazione.

.. rubric:: Footnotes

.. [#name] Testo di prova della nota a piè di pagina.
